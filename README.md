# Amitabha

n. <梵>（佛）阿弥陀佛

> 佛曰: 因果

> 那么一天

> 看见文章1.

> 发现还有2.这么个中间件

> 于是追寻源码,发现了3.

> 再然后寻思自己也可以写一个什么中间件,以便学习掌握

> 于是便有了*Amitabha*

## 因果线

- 1.[asp.net core 教程（六）-中间件](https://www.cnblogs.com/kejie/p/7016116.html)
- 2.[Microsoft.AspNetCore.Diagnostics](https://www.nuget.org/packages/Microsoft.AspNetCore.Diagnostics)
- 3.[https://github.com/aspnet/Diagnostics](https://github.com/aspnet/Diagnostics)
- 4.[WelcomePage](https://github.com/aspnet/Diagnostics/tree/master/src/Microsoft.AspNetCore.Diagnostics/WelcomePage)

## 获取

- 建议通过 [NuGet](https://www.nuget.org/packages/Amitabha) 获取包并使用
- 当然通过 [Source Code](https://github.com/taadis/Amitabha) 获取使用也是可以的

### 示例

- 新建一个空的 `ASP.NET Core Web` 应用程序
- 获取 [Amitabha](https://www.nuget.org/packages/Amitabha)
- 在 `Startup.cs`中添加以下内容,运行后,就能看见我佛了...

``` csharp
// Startup.cs

using Amitabha;

public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
    // ...
    app.UseAmitabha();
    // ...    
}
```
